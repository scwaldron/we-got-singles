class MessagesController < ApplicationController
  before_action :authenticate_user!
  def new
  end

  def create
    @message = Message.create(
                            sender_id: current_user.id,
                            receiver_id: params[:user_id],
                            body: message_params[:body]
                          )
    if @message.valid?
      redirect_to User.find(params[:user_id]), notice: 'Your message has been sent!'
    else
      render :new
    end
  end

  private

  def message_params
    params.require(:message).permit(:body)
  end
end
