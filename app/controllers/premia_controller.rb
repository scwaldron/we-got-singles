class PremiaController < ApplicationController
  def create
    @premium = current_user.premia.new(premium_params)
    if  @premium.save
      redirect_to user_account_path(current_user)
    else
      render :new
    end
  end

  def destroy
    @premium = current_user.premia.find(params[:id])
    @premium.unsubscribe

    redirect_to user_account_path(current_user)
  end

  def new
    @user = current_user
    @premium = current_user.premia.new
  end

private
  def premium_params
    params.require(:premium).permit(:accept_terms_and_conditions)
  end
end
