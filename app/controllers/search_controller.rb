class SearchController < ApplicationController
  helper SearchHelper

  def index
    @matches = User.where(user_params).decorate
  end

  private

  def user_params
    params.require(:user).permit(:gender, :ethnicity)
  end
end
