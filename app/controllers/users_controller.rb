class UsersController < ApplicationController
  before_action :authenticate_user!
  def show
    @user = (params[:id] ? User.find(params[:id]) : current_user).decorate
  end

  def edit
  end

  def update
    if current_user.update(user_params)
      redirect_to :root
    else
      render :edit
    end
  end

  private

  def user_params
    params.require(:user).permit(:smoker, :drinker, :dob, :ethnicity, :bio, :height, :star_sign)
  end
end
