class UserDecorator < Draper::Decorator
  delegate_all

  def gender
    model.gender ? 'Male' : 'Female'
  end

  def smoker
    model.smoker ? 'Smoker' : 'Non Smoker'
  end

  def drinker
    model.drinker ? 'Drinker' : 'Non Drinker'
  end

  def premium_txt
    model.premium ? 'premium' : 'standard'
  end

  def height
    model.height != 0 ? model.height.to_s + 'cm' : 'No height Specified'
  end

  def ethnicity
    model.ethnicity != 'unspecified' ? model.ethnicity.titleize : 'No ethnicity specified'
  end

  def bio
    model.bio && model.bio != '' ? 'Bio: ' + model.bio : 'No bio specified'
  end
end
