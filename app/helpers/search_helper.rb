module SearchHelper
  def ethnicities
    [['Unspecified', nil], ['Caucasian', 'caucasian'], ['African', 'african'],
     ['Asian', 'asian'], ['Hispanic', 'hispanic'], ['Other', 'other']]
  end
end
