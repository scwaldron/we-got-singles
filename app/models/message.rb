class Message < ActiveRecord::Base
  belongs_to :sender, class_name: 'User', foreign_key: :sender_id
  belongs_to :receiver, class_name: 'User', foreign_key: :receiver_id
  belongs_to :parent, class_name: 'Message', foreign_key: :parent_id
  has_one :child, class_name: 'Message'

  validates_presence_of :body
end
