class Premium < ActiveRecord::Base
  belongs_to :user
  validates :accept_terms_and_conditions, acceptance: true, allow_nil: false

  after_create :activate_premium

  def activate_premium
    self.user.update(premium: self)
  end

  def unsubscribe
    self.update(active: false)
    self.user.update(premium: nil)
  end
end
