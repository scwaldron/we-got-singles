class User < ActiveRecord::Base
  has_many :premia
  belongs_to :premium
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable,
         :validatable, :confirmable

  validates :username,
            format: { with: /\A\w{3,20}\z/,
            message: 'username must be 3 to 20 characters long'}

  validates_length_of :bio, maximum: 500

  validates_numericality_of :height, only_integer: true
  validates_inclusion_of    :height,
                            in: 0..280,
                            message: "can only be between 0 and 280."

  validates_inclusion_of  :star_sign, in: %w(aries taurus gemini cancer leo virgo libra scorpio sagittarius capricorn aquarius pisces), allow_nil: true

  validates_inclusion_of  :dob,
                          in: Date.new(1900)..Time.now.years_ago(18).to_date,
                          message: 'You must be over 18 to register for this service'

  validates_inclusion_of :ethnicity,
                         in: %w(unspecified caucasian african asian hispanic other)
end
