Rails.application.routes.draw do

  devise_for :users
  root to: 'users#show'
  resources :users, only: [:edit, :update, :show] do
    get 'account'
    resources :premia, shallow: true
  end
  resources :search
  resources :users, only: :show do
    resources :messages
  end
end
