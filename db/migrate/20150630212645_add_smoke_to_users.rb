class AddSmokeToUsers < ActiveRecord::Migration
  def change
    add_column :users, :smoke, :boolean, default: false
  end
end
