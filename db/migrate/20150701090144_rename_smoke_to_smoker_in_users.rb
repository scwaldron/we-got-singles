class RenameSmokeToSmokerInUsers < ActiveRecord::Migration
  def change
    rename_column :users, :smoke, :smoker
  end
end
