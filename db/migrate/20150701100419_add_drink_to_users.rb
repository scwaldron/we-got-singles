class AddDrinkToUsers < ActiveRecord::Migration
  def change
    add_column :users, :drink, :boolean, default: false
  end
end
