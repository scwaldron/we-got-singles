class ChangeUsersColumnDrinkToDrinker < ActiveRecord::Migration
  def change
    rename_column :users, :drink, :drinker
  end
end
