class ChangeBioTypeToString < ActiveRecord::Migration
  def change
    change_column :users, :bio, :string
  end
end
