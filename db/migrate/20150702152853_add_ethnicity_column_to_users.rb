class AddEthnicityColumnToUsers < ActiveRecord::Migration
  def change
    add_column :users, :ethnicity, :string, default: 'unspecified'
  end
end
