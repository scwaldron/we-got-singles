class AddStarSignToUsers < ActiveRecord::Migration
  def change
    add_column :users, :star_sign, :string, default: 'Cancer'
  end
end
