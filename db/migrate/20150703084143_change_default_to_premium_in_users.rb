class ChangeDefaultToPremiumInUsers < ActiveRecord::Migration
  def change
    change_column_default :users, :premium, false
  end
end
