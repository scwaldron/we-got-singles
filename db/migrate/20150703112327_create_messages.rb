class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.references :sender
      t.references :receiver
      t.references :parent
      t.boolean :read, null: false, default: false
      t.string :body, null: false, default: ""

      t.timestamps null: false
    end
  end
end
