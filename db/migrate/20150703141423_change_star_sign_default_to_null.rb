class ChangeStarSignDefaultToNull < ActiveRecord::Migration
  def up
    change_column :users, :star_sign, :string, default: nil
  end

  def down
  end
end
