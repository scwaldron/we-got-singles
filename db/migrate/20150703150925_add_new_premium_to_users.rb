class AddNewPremiumToUsers < ActiveRecord::Migration
  def change
    add_reference :users, :premium, index: true, foreign_key: true
  end
end
