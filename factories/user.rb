FactoryGirl.define do
  factory :user do
    gender { [true, false].sample }
    dob { Faker::Date.between(Date.new(1900), 18.years.ago) }
    username { SecureRandom.hex((2..10).to_a.sample) }
    email { Faker::Internet.free_email }
    password { Faker::Internet.password(8) }
    ethnicity { ['unspecified', 'caucasian', 'african', 'asian', 'hispanic', 'other'].sample }

    factory :male_user do
      gender  true
    end

    factory :female_user do
      gender  false
    end

    factory :premium_member do
      after(:create) do |user|
        user.premia.create(:accept_terms_and_conditions => "1")
      end
    end

    before(:create) { |user| user.confirm }
  end
end
