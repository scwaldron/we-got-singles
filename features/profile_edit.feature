Feature: A User enters their profile details
  As a User
  I want to fill in my details
  So other Users know about me

  Scenario: User indicates their smoking preference
    Given I am a registered and confirmed User
    When I am on the login page
      And I fill in my Email and Password
      And I submit my details
      And I click edit
      And I provide my smoking preference
      And I click update
    Then my preferences are saved
      And I am directed to my profile

  Scenario: User indicates their drinking preference
    Given I am a registered and confirmed User
    When I am on the login page
      And I fill in my Email and Password
      And I submit my details
      And I click edit
      And I provide my drinking preference
      And I click update
    Then my drinking preferences are saved
      And I am directed to my profile

  Scenario: User adds a bio to their profile
    Given I am a registered and confirmed User
    When I am on the login page
      And I fill in my Email and Password
      And I submit my details
      And I click edit
      And I fill in my Bio
      And I click update
    Then my Bio is saved
      And I am directed to my profile

  Scenario: User adds their height to their profile
    Given I am a registered and confirmed User
    When I am on the login page
      And I fill in my Email and Password
      And I submit my details
      And I click edit
      And I enter my height
      And I click update
    Then my height is saved
      And I am directed to my profile

  Scenario: User provides their date of birth
    Given I am a registered and confirmed User
    When I am on the login page
      And I fill in my Email and Password
      And I submit my details
      And I click edit
      And I provide my Date of Birth
      And I click update
    Then my date of birth is saved
      And I am directed to my profile

  Scenario: User adds their star sign to their profile
    Given I am a registered and confirmed User
    When I am on the login page
      And I fill in my Email and Password
      And I submit my details
      And I click edit
      And I select my Star Sign
    When I click update
    Then my Star Sign is saved
      And I am directed to my profile
