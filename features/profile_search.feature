Feature: A User searches for matches
  As a User
  I want to search for matches by gender
  So I can find my perfect match

  Scenario: User successfully search for matches by gender
    Given I am a registered and confirmed User
      And there are other users registered
    When I am on the login page
      And I fill in my Email and Password
      And I submit my details
      And I am directed to my profile
      And I click on search link
    Then I am directed to my search
      And I choose female in dropdown list
      And I click on search
      And I can see list of users for that gender

  Scenario: User successfully search for matches by ethnicity
    Given I am a registered and confirmed User
      And there are other users registered
    When I am on the login page
      And I fill in my Email and Password
      And I submit my details
      And I am directed to my profile
      And I click on search link
    Then I am directed to my search
      And I choose an ethnicity from the dropdown list
      And I click on search
      And I can see list of users for that ethnicity
