Feature: A customer sees User profile
  As a customer
  I want see User profiles
  So I can see if they are the one

  Scenario: Customer sees a profile
    Given I am a logged in User
    When I visit a User profile
    Then I can see the Username
      And I can see the Gender
      And I can see if they Drink
      And I can see if they Smoke
