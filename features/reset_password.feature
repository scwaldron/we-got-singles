Feature: A customer resets their password
  As a customer
  I want to reset my password
  So I can log in

  Scenario: Resetting your password
    Given I am a registered and confirmed User
      And I am on the login page
    When I click 'Forgot your password?'
      And I enter my email address
      And I click 'Send me reset password instructions'
    Then I receive a password reset email
    When I click 'Change password'
    Then I am redirected to the reset password page
      And I enter my new password
    When I submit my new password
    Then I am logged in
      And I am directed to my profile
