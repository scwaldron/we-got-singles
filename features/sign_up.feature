Feature: A Customer signs up
  As a customer
  I want to sign up
  So I can find that special someone

  Scenario: Successfully signing up
    Given there are no users
    When I am on the sign up page
      And I select my gender
      And I fill in my DoB
      And I fill in my Username
      And I fill in my Email
      And I fill in my Password
      And I submit my information
    Then my account is created
      And I receive a confirmation email

  Scenario: Confirming my User account
    Given I have registered
    When I follow the link within my confirmation email
    Then my account is confirmed
      And I am directed to the log in page
