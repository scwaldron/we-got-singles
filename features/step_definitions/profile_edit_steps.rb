When(/^I click edit$/) do
  click_link 'Edit Profile'
end

When(/^I provide my smoking preference$/) do
  within('.user-smokes') do
    choose 'Yes'
  end
end

When(/^I provide my drinking preference$/) do
  within('.user-drinks') do
    choose 'Yes'
  end
end

When(/^I provide my Date of Birth$/) do
  fill_in 'Date of Birth', with: '1990/04/21'
end

When(/^I click update$/) do
  click_button 'Update User'
end

Then(/^my preferences are saved$/) do
  expect(User.last).to be_smoker
end

Then(/^my drinking preferences are saved$/) do
  expect(User.last).to be_drinker
end

When(/^I fill in my Bio$/) do
  fill_in 'user_bio', with: 'Testing profile bio'
end

Then(/^my Bio is saved$/) do
  expect(User.last.bio).to eq('Testing profile bio')
end

When(/^I enter my height$/) do
 fill_in 'Height', with: 147
end

Then(/^my height is saved$/) do
  expect(User.last.height).to eq(147)
end

Then(/^my date of birth is saved$/) do
  expect(User.last.dob.to_s).to eq('21/04/1990')
end

When(/^I select my Star Sign$/) do
  select 'Aries', from: 'user_star_sign'
end

Then(/^my Star Sign is saved$/) do
  expect(User.last.star_sign).to eq('aries')
end
