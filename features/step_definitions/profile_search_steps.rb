Given(/^there are other users registered$/) do
  @female_user = FactoryGirl.create(:female_user)
  @male_user = FactoryGirl.create(:male_user)
end

When(/^I click on search link$/) do
  click_link 'search'
end

Then(/^I am directed to my search$/) do
  visit new_search_path
end

When(/^I choose female in dropdown list$/) do
  select 'Female', from: 'user_gender'
  select @female_user.ethnicity.capitalize, from: 'user_ethnicity'
end

When(/^I click on search$/) do
  click_button 'Search'
end

Then(/^I can see list of users for that gender$/) do
  expect(page.body).to have_content(@female_user.username)
end

Then(/^I choose an ethnicity from the dropdown list$/) do
  select 'Male', from: 'user_gender'
  select @male_user.ethnicity.capitalize, from: 'user_ethnicity'
end

Then(/^I can see list of users for that ethnicity$/) do
  expect(page.body).to have_content(@male_user.username)
end
