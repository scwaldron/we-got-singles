
When(/^I am a (\w+)?\s?logged in User$/) do |user_type|
  user_type = (user_type || :user).to_sym
  @user = FactoryGirl.create(user_type)
  visit new_user_session_path

  fill_in 'Email', with: @user.email
  fill_in 'Password', with: @user.password
  click_button 'Log in'
end

When(/^I visit a User profile$/) do
  @other_user = FactoryGirl.create(:user).decorate
  visit url_for(@other_user)
end

Then(/^I can see the Username$/) do
  expect(page.body).to have_content(@other_user.username)
end

Then(/^I can see the Gender$/) do
  expect(page.body).to have_content(@other_user.gender)
end

Then(/^I can see if they Drink$/) do
  expect(page.body).to have_content(@other_user.drinker)
end

Then(/^I can see if they Smoke$/) do
  expect(page.body).to have_content(@other_user.smoker)
end
