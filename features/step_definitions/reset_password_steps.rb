Given(/^I click 'Forgot your password\?'$/) do
  click_link 'Forgot your password?'
end

Given(/^I enter my email address$/) do
  fill_in 'Email', with: @user.email
end

Given(/^I click 'Send me reset password instructions'$/) do
  click_button 'Send me reset password instructions'
end

Then(/^I receive a password reset email$/) do
  expect(ActionMailer::Base.deliveries).not_to be_empty
end

When(/^I click 'Change password'$/) do
  @email = Nokogiri::HTML(ActionMailer::Base.deliveries.first.body.raw_source)
  visit @email.css('a').first.attributes['href'].value
end

Then(/^I am redirected to the reset password page$/) do
  expect(page).to have_content 'Change your password'
end

Then(/^I enter my new password$/) do
  fill_in 'user_password', with: 'g13nsm1th'
  fill_in 'user_password_confirmation', with: 'g13nsm1th'
end

When(/^I submit my new password$/) do
  click_button 'Change my password'
end

Then(/^I am logged in$/) do
  expect(page).to have_content 'signed in'
end
