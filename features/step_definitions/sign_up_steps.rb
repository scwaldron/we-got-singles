Given(/^there are no users$/) do
  expect(User.all).to be_empty
end

When(/^I am on the sign up page$/) do
  visit new_user_registration_path
  @user1 = FactoryGirl.build(:user)
end

When(/^I select my gender$/) do
  choose 'Male'
end

When(/^I fill in my DoB$/) do
  fill_in 'Date of Birth', with: @user1.dob
end

When(/^I fill in my Username$/) do
  fill_in 'Username', with: @user1.username
end

When(/^I fill in my Email$/) do
  fill_in 'Email', with: @user1.email
end

When(/^I fill in my Password$/) do
  fill_in 'Password', with: @user1.password
  fill_in 'Password confirmation', with: @user1.password
end

When(/^I submit my information$/) do
  click_button 'Sign up'
end

Then(/^my account is created$/) do
  expect(User.last.username).to eq(@user1.username)
end

Then(/^I receive a confirmation email$/) do
  expect(ActionMailer::Base.deliveries).not_to be_empty
end

Given(/^I have registered$/) do
  @user = FactoryGirl.create(:user)
end

When(/^I follow the link within my confirmation email$/) do
  visit user_confirmation_path(confirmation_token: @user.instance_variable_get(:@raw_confirmation_token))
end

Then(/^my account is confirmed$/) do
  expect(User.first).to be_confirmed
end

Then(/^I am directed to the log in page$/) do
  expect(page).to have_content 'Log in'
end
