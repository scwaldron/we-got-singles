When(/^I am in my edit profile$/) do
  visit edit_user_path(@user)
end

When(/^I click Account$/) do
  click_link 'Account'
end

When(/^I click Subscribe$/) do
  click_link 'Subscribe'
end

When(/^I accept terms and conditions$/) do
  check('Accept terms and conditions')
end

When(/^I submit my subscrption$/) do
  click_button 'Create Premium'
end

Then(/^I become a Premium member$/) do
  expect(page.body).to have_content('You are a premium account holder')
end

When(/^I click Unsubscribe$/) do
  click_link 'Unsubscribe'
end

Then(/^I am no longer a Premium member$/) do
    expect(page.body).to have_content('You currently have a Standar account')
end
