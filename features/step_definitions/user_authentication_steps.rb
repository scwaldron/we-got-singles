Given (/^I am a registered and confirmed User$/) do
  @user = FactoryGirl.create(:user)
end

Given(/^I am on the login page$/) do
  visit new_user_session_path
end

Given(/^I fill in my Email and Password$/) do
  fill_in 'Email', with: @user.email
  fill_in 'Password', with: @user.password
end

Given(/^I submit my details$/) do
  click_button 'Log in'
end

Then(/^I am directed to my profile$/) do
  expect(page).to have_content("#{@user.username}'s profile")
end

Then(/^I click the sign out link$/) do
  click_link 'Sign out'
end

Then(/^I am signed out$/) do
  expect(page).to have_content 'Log in'
end
