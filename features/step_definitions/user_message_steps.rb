When(/^I click the message button$/) do
  click_button 'Send Message'
end

When(/^I type in a message and click Send Message$/) do
  fill_in 'Message:', with: 'I love you!'

  click_button 'Send Message'
end

Then(/^I see a confirmation and my message is sent$/) do
  expect(page).to have_content('Your message has been sent!')
  expect(Message.last.body).to eq('I love you!')
end
