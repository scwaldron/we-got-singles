Feature: A Customer authenticates
  As a customer
  I want to sign in
  So I can identify myself

  Scenario: Successful sign in/out
    Given I am a registered and confirmed User
      And I am on the login page
      And I fill in my Email and Password
    When I submit my details
    Then I am directed to my profile
    When I click the sign out link
    Then I am signed out
