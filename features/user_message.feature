Feature: User can send another user a message
  As a User
  I want to send messages
  So that I can talk to people I'm interested in

  Scenario: Successfully sending a message
    Given I am a logged in User
    When I visit a User profile
    And I click the message button
    And I type in a message and click Send Message
    Then I see a confirmation and my message is sent
