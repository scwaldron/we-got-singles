Feature: User subscribes to Premium
  As a User
  I want to subscribe to Premium
  So I can send messages to other Users

  Scenario: User subcribes to a Premium account
    Given I am a logged in User
    When I click Account
      And I click Subscribe
      And I accept terms and conditions
      And I submit my subscrption
    Then I become a Premium member

