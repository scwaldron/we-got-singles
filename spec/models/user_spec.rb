require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'validation' do
    let(:user) do
      FactoryGirl.build(:user)
    end

    it 'is valid with valid attributes' do
      expect(user).to be_valid
    end

    it 'expects the username to be between 3 to 20' do
      user.username = 'ra'
      expect(user).to_not be_valid

      user.username = 'ra34567890123456789012'
      expect(user).to_not be_valid

      user.username = 'rayy'
      expect(user).to be_valid
    end

    it 'expects the age to be over 18' do
      user.dob = '12/12/2000'
      expect(user).to_not be_valid
    end

    it 'limits the profile bio to 500 characters' do
      user.bio = Faker::Lorem.paragraphs(4)
      expect(user).to_not be_valid
    end

    it 'expects height to be an integer' do
      user.height = 148
      expect(user).to be_valid

      user.height = 'the'
      expect(user).to_not be_valid
    end

    it 'expects height to be between 0 and 280' do
      user.height = -23
      expect(user).to_not be_valid

      user.height = 300
      expect(user).to_not be_valid

      user.height = 56
      expect(user).to be_valid
    end

    it 'expects the star sign to be valid' do
      user.star_sign = 'cancer'
      expect(user).to be_valid

      user.star_sign = 'bad'
      expect(user).to_not be_valid
    end
  end
end
