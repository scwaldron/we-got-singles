Feature: User unsubscribe from their Premium account
  As a User
  I want to unsubscribe from Premium account
  So I no longer pay as I don´t need it

  Scenario: User unsubcribes to a Premium account
    Given I am a premium_memeber logged in User
    When I click Account
      And I click Unsubscribe
    Then I am no longer a Premium member

